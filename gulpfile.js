var gulp = require('gulp');
    sass = require('gulp-sass');
    autoprefixer = require('gulp-autoprefixer');
    browserSync = require('browser-sync').create();
    server = require('gulp-server-livereload');
    minifyCSS = require('gulp-clean-css');
    minifyJS = require('gulp-minify');

gulp.task('sass', function(){
  return gulp.src('./assets/sass/*.sass')
  .pipe(sass().on('error', sass.logError))
  .pipe(autoprefixer())
  .pipe(gulp.dest('assets/styles'))
  .pipe(minifyCSS({compatibility: 'ie8'}))
  .pipe(browserSync.stream());
});
gulp.task('browser-sync', function(){
  browserSync.init({
    proxy: '192.168.33.10'
  });
});
gulp.task('minifyJS', function(){
  gulp.src('compiled/main.js')
    .pipe(minifyJS({
      ext: {
        src: '-orig.js',
        min: '-min.js'
      }
    }))
    .pipe(gulp.dest('/'))
});
gulp.task('webserver', function(){
  gulp.src('')
    .pipe(server({
      livereload: true,
      open: true,
      port: 9580
    }));
});
gulp.task('watch', function(){
  gulp.watch('./assets/sass/*.sass', ['sass', browserSync.stream]);
  gulp.watch('./*.html').on('change', browserSync.reload);
});
gulp.task('default',['sass', 'watch', 'browser-sync']);
