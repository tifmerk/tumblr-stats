let webpack = require('webpack');
let path = require('path');
let app = 'main';
let vue = './assets/js/app.js';
let exportloc = path.resolve(__dirname, './complied');
let plugins = [];
let env = process.env.WEBPACK_ENV;

if(env === 'production'){
  let UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;

  plugins.push(new UglifyJsPlugin());
  plugins.push(new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production')
    }
  }));
  app = app + '.min.js';
}else{
  app = app + '.js';
}

module.exports = {
  entry: vue,
  output: {
    path: exportloc,
    filename: app
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader'
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
    plugins
};
